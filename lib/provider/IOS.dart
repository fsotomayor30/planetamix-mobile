import 'package:flutter/foundation.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:cloud_firestore/cloud_firestore.dart';


class IOSProvider with ChangeNotifier {

  String url="http://streaming.comunicacioneschile.net\u{3A}9326";
  bool isLocal=false;
  AudioPlayer audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
  String urlMusic;
  String activado;
  bool escuchando=false;

  IOSProvider() {
    CollectionReference configurationRef = Firestore.instance.collection("Musica");
    configurationRef.snapshots().listen((querySnapshot) {
      querySnapshot.documentChanges.forEach((document) {
        activado = document.document['activado'];
        urlMusic = document.document['url'];
      });
    });
  }

  Future<int> play() async {
    print("URL MUSICA: "+urlMusic);
    print("ACTIVADO: "+activado);
    if(activado=="Verdadero"){
      escuchando = true;
      await audioPlayer.play(url, isLocal: isLocal, position: null);
    }else{
      escuchando = true;
      await audioPlayer.play(urlMusic, isLocal: isLocal, position: null);
    }

    notifyListeners();

  }

  Future<int> stop() async {
    await audioPlayer.stop();
    print("stop");
    escuchando = false;

    notifyListeners();
  }

}