
import 'package:flutter/foundation.dart';
import 'package:flutter_radio/flutter_radio.dart';
import 'package:intl/intl.dart';
import 'package:cloud_firestore/cloud_firestore.dart';



class AndroidProvider  with ChangeNotifier {
bool escuchando=false;
String urlMusic;
String activado;

  AndroidProvider(){

    CollectionReference configurationRef = Firestore.instance.collection("Musica");
    configurationRef.snapshots().listen((querySnapshot) {
      querySnapshot.documentChanges.forEach((document) {
        activado = document.document['activado'];
        urlMusic = document.document['url'];
      });
    });

  audioStart();
  }

  Future<void> audioStart() async {
    await FlutterRadio.audioStart();
    escuchando=false;
    print('Audio Start OK');
  }

  Future<int> play() async {
    print("URL MUSICA: "+urlMusic);
    print("ACTIVADO: "+activado);
    if(activado=="Verdadero"){
      FlutterRadio.play(url: "http://streaming.comunicacioneschile.net\u{3A}9326");
    }else{
      FlutterRadio.play(url: urlMusic);
    }
    escuchando=true;
    notifyListeners();

  }

  Future<int> stop() async {
    FlutterRadio.stop();
    escuchando=false;
    notifyListeners();

  }

}

