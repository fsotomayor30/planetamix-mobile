import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';


class TeamScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => new Team();
}

class Team extends State<TeamScreen> {


  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return Scaffold(
      body: Container(
        height: mediaQueryData.height,

        decoration: BoxDecoration(
          color: Color.fromRGBO(0,0,0,1),
        ),
        child: ListView(
          children: [
            Container(
              decoration: BoxDecoration(
                color: Color.fromRGBO(0,0,0,1),
              ),
              width: mediaQueryData.width,
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Conductor de Radio",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'fira-sans',
                              fontSize: 25
                          ),
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("David Chávez",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontFamily: 'fira-sans',
                              fontSize: 50,
                              fontWeight: FontWeight.bold
                          ),),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: mediaQueryData.width*0.9,
                          child: Text("Comunicador, Líder Coaching, Radio Host, Conductor de Radio"
                              ", Animador Escénico, Docente, Profesion: Licenciado en Trabajo Social.",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontFamily: 'fira-sans',
                                color: Colors.white,
                                fontSize: 18
                            ),
                            overflow:
                            TextOverflow.ellipsis,
                            maxLines: 10,),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.asset("assets/Imagenes/david_chavez.png",
                            width: mediaQueryData.width-36,),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("En Instagram como: ",textAlign: TextAlign.center,
                          style: TextStyle(
                              fontFamily: 'fira-sans',
                              color: Colors.white,
                              fontSize: 18,
                              fontWeight: FontWeight.bold

                          ),
                          overflow:
                          TextOverflow.ellipsis,
                          maxLines: 10,),

                      ],
                    ),
                    GestureDetector(
                      onTap: () async {
                        if (await canLaunch("https://www.instagram.com/david_chavezc")) {
                          await launch("https://www.instagram.com/david_chavezc");
                        }
                      },
                      child:Padding(
                        padding: const EdgeInsets.fromLTRB(5, 0, 5, 0),
                        child: Column(
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.asset("assets/Imagenes/instagram.png",
                                    width: 50,),
                                ),
                                Text("david_chavezc",textAlign: TextAlign.center,
                                  style: TextStyle(
                                      fontFamily: 'fira-sans',
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontWeight: FontWeight.bold

                                  ),
                                  overflow:
                                  TextOverflow.ellipsis,
                                  maxLines: 10,
                                ),
                              ],
                            ),

                          ],
                        ),
                      ),
                    ),


                  ],
                ),
              ),
            ),
          ],
        ),
      )
    );
  }
}
