import 'package:flutter/material.dart';
import 'package:planetamix/module/Inicio.dart';
import 'package:planetamix/module/clientes/Clientes.dart';
import 'package:planetamix/module/programa/Programa.dart';
import 'package:planetamix/module/team/Team.dart';
import 'package:planetamix/provider/Android.dart';
import 'package:planetamix/provider/IOS.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:provider/provider.dart';//es la dependencia que importamos.
import 'dart:io';


class MenusPage extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Widget e;
    if(Platform.isAndroid){
/*
      e= Container();
*/

        e=ChangeNotifierProvider<AndroidProvider>(
          create: (context) => AndroidProvider() ,
          child: Scaffold(
            body: MenuList(),
          ),
        );


    }else{
      if(Platform.isIOS){
/*
        e= Container();
*/
        e=ChangeNotifierProvider<IOSProvider>(
              create: (context) => IOSProvider() ,
              child: Scaffold(
                body: MenuList(),
              ),
            );
      }
    }

    return e;
  }
}

class MenuList extends StatelessWidget{

  List<Widget> containers = [
    Container(
      child: InicioScreen()),
    Container(
      child: TeamScreen()),
      //child: OptionList(ListTiempoLibre),
    Container(
      child: ClientesScreen(),
    )
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(0,0,0,1),
          title: Row(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 10, 0, 0),
                child: Image.asset('assets/Imagenes/logo.png', width: 100,),
              ),
             // Text('iChillán'),
            ],
          ),
          bottom : TabBar(indicatorColor: Colors.white,tabs: <Widget>[

            Container(
              child: Tab(
                  text: 'Radio'
              ),
            ),
            Tab(
                text: 'Team'
            ),
            Tab(
                text: 'Clientes'
            )
          ]),
        ),
        body: TabBarView(children: containers),
        drawer: Drawer(
          child: Container(
            decoration: BoxDecoration(
              color: Color.fromRGBO(19,19,19,1),
            ),
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                DrawerHeader(
                  child: Image.asset('assets/Imagenes/logo.png'),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(19,19,19,1),
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(19,19,19,1),
                  ),
                  child: ListTile(
                    title: Text('Contacto y publicidad', style: TextStyle(color: Colors.white),),
                    leading: CircleAvatar(
                      child: Image.asset('assets/Imagenes/contacto.png'),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: () async {
                      if (await canLaunch("mailto:davidchavez@planetamixchile.cl?subject=Contacto")) {
                        await launch("mailto:davidchavez@planetamixchile.cl?subject=Contacto");
                      }
                    },
                  ),
                ),

                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(19,19,19,1),
                  ),
                  child: ListTile(
                    title: Text('Radio Planeta Mix', style: TextStyle(color: Colors.white),),
                    leading: CircleAvatar(
                      child: Image.asset('assets/Imagenes/click.png'),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: () async {
                      if (await canLaunch("https://planetamixchile.cl/")) {
                        await launch("https://planetamixchile.cl/");
                      }
                    },
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(19,19,19,1),
                  ),
                  child: ListTile(
                    title: Text('Programa', style: TextStyle(color: Colors.white),),
                    leading: CircleAvatar(
                      child: Image.asset('assets/Imagenes/auriculares.png'),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ProgramaScreen()),
                      );
                    },
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(19,19,19,1),
                  ),
                  child: ListTile(
                    title: Text('Facebook', style: TextStyle(color: Colors.white),),
                    leading: CircleAvatar(
                      child: Image.asset('assets/Imagenes/facebook.png'),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: () async {
                      if (await canLaunch("https://www.facebook.com/profile.php?id=100010277293346")) {
                        await launch("https://www.facebook.com/profile.php?id=100010277293346");
                      }
                    },
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(19,19,19,1),
                  ),
                  child: ListTile(
                    title: Text('Instagram', style: TextStyle(color: Colors.white),),
                    leading: CircleAvatar(
                      child: Image.asset('assets/Imagenes/instagram.png'),
                      backgroundColor: Colors.transparent,
                    ),
                    onTap: () async {
                      if (await canLaunch("https://www.instagram.com/planetamixchile")) {
                        await launch("https://www.instagram.com/planetamixchile");
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



