import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';


class ProgramaScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => new Programa();
}

class Programa extends State<ProgramaScreen> {


  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(19,19,19,1)),
      body: SingleChildScrollView(
        child: Stack(
            children: <Widget>[
      Container(
        //width: mediaQueryData.width*0.9,
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(0, 10, 0 ,0),
                    child: Text("Radio Show",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'fira-sans',
                          fontSize: 25
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text('"Planeta Mix"',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontFamily: 'fira-sans',
                        fontSize: 50,
                        fontWeight: FontWeight.bold
                    ),),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: mediaQueryData.width*0.9,
                    child: Text("Trae lo mejor de los sonidos de moda en una selección que incluye música nacional, Electrónica, Pop y Reggaetón, de permanente buen humor. \nDentro de las voces de Radio Isadora se encuentran David Chávez y más! 13 años junto a ti entregando la mejor energía del centro sur del país, escúchanos de Lunes a Sábado de 15:30 a 17:30hrs.",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontFamily: 'fira-sans',
                          fontSize: 18
                      ),
                      overflow:
                      TextOverflow.ellipsis,
                      maxLines: 10,),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.asset("assets/Imagenes/planetamix.jpeg",
                      width: mediaQueryData.width*0.9,),
                  )
                ],
              ),

            ],
          ),
        ),
      ),
            ],
        ),
      )
    );

  }
}
