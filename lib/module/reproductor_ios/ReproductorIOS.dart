import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:planetamix/provider/IOS.dart';
import 'package:provider/provider.dart';


class ReproductorIOS extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final iosProvider=Provider.of<IOSProvider>(context);
    final mediaQueryData = MediaQuery.of(context).size;

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Card(
        elevation: 10,
        shadowColor: Colors.white,
        child: Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                  width: (mediaQueryData.width*.5)-32,
                  height: (mediaQueryData.width*.5)-32,
                  decoration: new BoxDecoration(
                      color: Colors.black),
                  child: Image.asset("assets/Imagenes/logo.png")),
            ),
            Container(
              width: (mediaQueryData.width*.5)-16,
              child: Column(
                children: [
                  FaIcon(FontAwesomeIcons.headphonesAlt),
                  Container(
                      width: (mediaQueryData.width*.5)-16,
                      child:
                      (!iosProvider.escuchando)?
                      AutoSizeText(
                        'Dale Play para escucharnos',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 10.0),
                        maxLines: 2,
                      ): AutoSizeText(
                        'Estas escuchando Planeta Mix',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 10.0),
                        maxLines: 2,
                      )
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        (!iosProvider.escuchando)
                            ? new RawMaterialButton(
                          onPressed: () => iosProvider.escuchando
                              ? null
                              : iosProvider.play(),
                          child: new Icon(
                            Icons.play_arrow,
                            color: Colors.white70,
                            size: 35.0,
                          ),
                          shape: new CircleBorder(),
                          elevation: 2.0,
                          fillColor: Colors.black54,
                          padding: const EdgeInsets.all(15.0),
                        )
                            : new RawMaterialButton(
                          onPressed: () => iosProvider.escuchando
                              ? iosProvider.stop()
                              : null,
                          child: new Icon(
                            Icons.stop,
                            color: Colors.white70,
                            size: 35.0,
                          ),
                          shape: new CircleBorder(),
                          elevation: 2.0,
                          fillColor: Colors.black54,
                          padding: const EdgeInsets.all(15.0),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}