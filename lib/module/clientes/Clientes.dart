import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ClientesScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new Clientes();
}

class Clientes extends State<ClientesScreen> {
  @override
  initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget buildListItem(BuildContext context, DocumentSnapshot document) {
    return new ListTile(
      title: (Text(document['Nombre'],
          style: TextStyle(
            color: Colors.white,
          ))),
      subtitle: (Text(document['Descripción'],
          style: TextStyle(
            color: Colors.white,
          ))),
      leading: Image.network(document['Imagen']),
      trailing: Icon(
        Icons.keyboard_arrow_right,
        color: Colors.white,
      ),
      onTap: () async {
        if (await canLaunch(document['Url'])) {
          await launch(document['Url']);
        }
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context).size;

    return Scaffold(
        body: Container(
      height: mediaQueryData.height,
      decoration: new BoxDecoration(color: Color.fromRGBO(19, 19, 19, 1)),
      child: StreamBuilder(
          stream: Firestore.instance.collection("Auspiciadores").snapshots(),
          builder: (context, snapshot) {
            if (!snapshot.hasData) return Container();
            return Column(
              children: <Widget>[
                Expanded(
                  child: ListView.builder(
                    //itemExtent: 80.0,
                    itemCount: snapshot.data.documents.length,
                    itemBuilder: (context, index) =>
                        buildListItem(context, snapshot.data.documents[index]),
                  ),
                ),
              ],
            );
          }),
    ));
  }
}
