import 'dart:io';
import 'package:audioplayers/audioplayers.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:planetamix/module/reproductor_android/ReproductorAndroid.dart';
import 'package:planetamix/module/reproductor_ios/ReproductorIOS.dart';
import 'package:url_launcher/url_launcher.dart';

typedef void OnError(Exception exception);

class InicioScreen extends StatefulWidget {
  @override
  _InicioState createState() => new _InicioState();
}

class _InicioState extends State<InicioScreen> {
  @override
  Widget build(BuildContext context) {
    Widget Radio;
    final mediaQueryData = MediaQuery.of(context).size;
    Widget w_eventos = StreamBuilder(
        stream: Firestore.instance.collection("Publicidad").snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) return Container();
          return Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              // shadowColor: Colors.white,
              elevation: 0,
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 15, bottom: 15),
                    child: AutoSizeText(
                      'Espacio Publicitario',
                      textAlign: TextAlign.center,
                      style: TextStyle(fontSize: 20.0),
                      maxLines: 2,
                    ),
                  ),
                  CarouselSlider(
                    options: CarouselOptions(
                      autoPlayInterval: Duration(seconds: 5),
                      autoPlayAnimationDuration: Duration(milliseconds: 800),
                      autoPlay: true,
                    ),
                    items: snapshot.data.documents
                        .map<Widget>((DocumentSnapshot document) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            height: 2,
                            margin: EdgeInsets.symmetric(horizontal: 5.0),
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(0, 4, 0, 4),
                              child: CachedNetworkImage(
                                imageUrl: document['url'],
                                placeholder: (context, url) =>
                                    CircularProgressIndicator(),
                                errorWidget: (context, url, error) =>
                                    Icon(Icons.error),
                              ),
                            ),
                          );
                        },
                      );
                    }).toList(),
                  ),
                ],
              ),
            ),
          );
        });
    if (Platform.isAndroid) {
      print("Dispositivo Android");
      Radio = Container(
        decoration: new BoxDecoration(color: Color.fromRGBO(0, 0, 0, 1)),
        height: mediaQueryData.height,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              ReproductorAndroid(),
              Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                child: w_eventos,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30),
                child: GestureDetector(
                    onTap: () async {
                      if (await canLaunch(
                          'https://www.instagram.com/ichillanapp/')) {
                        await launch('https://www.instagram.com/ichillanapp/');
                      }
                    },
                    child: Image.asset("assets/Imagenes/Banner ichillan.png")),
              ),
            ],
          ),
        ),
      );
    } else {
      if (Platform.isIOS) {
        print("Dispositivo IOS");
        Radio = Container(
          decoration: new BoxDecoration(color: Color.fromRGBO(0, 0, 0, 1)),
          height: mediaQueryData.height,
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                ReproductorIOS(),
                w_eventos,
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Image.asset("assets/Imagenes/Banner ichillan.png"),
                ),
              ],
            ),
          ),
        );
      }
    }
    return Radio;
  }
}
