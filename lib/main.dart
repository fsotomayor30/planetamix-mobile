import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:planetamix/module/menu/Menu_list_view.dart';
import 'package:splashscreen/splashscreen.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(new MaterialApp(
    home: new Inicio(),
  ));
}

class Inicio extends StatefulWidget {
  @override
  _MyAppState createState() => new _MyAppState();
}
class _MyAppState extends State<Inicio> {
  @override
  Widget build(BuildContext context) {
    return new SplashScreen(
        seconds: 4,
        navigateAfterSeconds: new MenusPage(),
        image: new Image.asset("assets/Imagenes/logosplash.jpeg"),
        backgroundColor: Colors.white,
        styleTextUnderTheLoader: new TextStyle(),
        photoSize: 120,
        //onClick: ()=>print("Flutter Egypt"),
        loaderColor: Colors.red
    );
  }
}
